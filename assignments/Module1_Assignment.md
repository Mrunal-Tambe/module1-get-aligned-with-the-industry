# INDUSTRIAL PRODUCT STUDY
## 1. Product Name - Smart Bio Lock
- **Model** - FL 1000
- **Domain** - Security and Surviellance 
- **Category**- Lock Accessory, Face Lock

**Product Link** - [FL 1000 eSSL Security](http://www.esslsecurity.com/product/fl1000)

### Manufacturer
eSSL Security

### Short Description
A smart door lock with embedded face recognition technology which uses new generation face recognition algorithm.

### Specifications
- Features: 1) Person Detection 2) ID Recognition
- 4 Unlocking ways: 1) Face Recognition 2) Access Card 3) Password 4) Mechanical key
- Storage Capacity: 100 each for faces,cards abd passwords.
- Speed: Less than 1.5sec
- Capacitive touch screen

### Other Relevant Products
Time Access B10 - A, BioFace MSD1K


# 2. Product Name - Indoor Home Security Camera 
- **Model** - SimCam 1S AI 
- **Domain** - Security and Surviellance 
- **Category**- AI based Surviellance Camera

**Product Link** - [SimCam 1S AI ](https://store.simcam.ai/products/home-security-camera-simcam-1s)

### Manufacturer
SIMCAM

### Short Description
An indoor security camera with span of 360° for theft or intruder detection and AI based motion sensing.

### Specifications
- Features: 1) Person detection 2) Object detection 3) Face recognition
- Night vision enabled.
- Notifies when intruder breaks into the house or a family member enter the house.
- Motion sensing upto 60 feet - Follows the valuable objects when suspicious movement is detected.
- Event recording upto 60sec
- Reduced false alarms.

### Other Relevant Products
SimCam Alloy 1S, SimCam 1S & SimCam Alloy 1S System

# 3. Product Name - Biometric Attendance 
- **Model** - Realtime T52F 
- **Domain** - AI based Biometrics
- **Category**- Biometric Attendance

**Product Link** - [Realtime T52F](http://www.realtimebiometrics.com/product-detail.php?id=129&title=Realtime-T52F)

### Manufacturer
Realtime Biometrics

### Short Description
A device for biometric attendance cum accesss system based on face recognition, fingerprint matching etc in corporate companies.

### Specifications
- Features: 1) Face recognition 2) RFID recognition
- Storage for faces,fingerprints and RFID cards - 5000 each
- Voice output interface
- Internet enabled, cloud connectivity
- Dual camera

### Other Relevant Products
StarLabs Realtime C101, TIME Access FACE Recognition 
